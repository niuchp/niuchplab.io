---
layout: post
title: 利用gitlab构建jekyll个人博客
categories: [jekyll]
description: 利用gitlab构建jekyll个人博客
keywords: gitlab, jekyll
---

> 很早以前用Travis CI做了博客的自动构建和部署[用 Travis CI 免费自动构建和部署 Jekyll](https://kago.site/2018/01/25/travis-ci-for-jekyll/)，后来一直没去更新、维护博客，再用的时候发现Travis CI中之前的项目都找不到了，折腾了半天没能让新的Travis CI(https://app.travis-ci.com/)识别出来我的github.io结尾的项目，除了这个项目都识别出来了，脑瓜疼。上网搜了下gitlab也能做，而且还是git库、CI/CD、SSL证书都有，记录一下抢救过程。


## 1、基础配置

### 1.1、导入项目

在[gitlab](https://gitlab.com/projects/new)导入github中的项目，导入项目后记得在"Settings->General"中修改项目名称和项目路径，以"gitlab.io"结尾，和github一样。

![导入项目](/images/posts/gitlab/导入项目.png)

### 1.2、修改配置

点击“CI/CD”，会弹出修改.gitlab-ci.yml，参考官方给的jekyll样例修改即可。
```yaml
image: ruby:2.5.9

variables:
  JEKYLL_ENV: production
  LC_ALL: C.UTF-8

before_script:
  - gem install bundler
  - bundle install

test:
  stage: test
  script:
  - bundle exec jekyll build -d test
  artifacts:
    paths:
    - test
  except:
  - master

pages:
  stage: deploy
  script:
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master

```

### 1.3、配置Runners

官方免费的Runners是共享的，还需要验证信号卡，虽然提示不会扣费但是还是有点不放心，正好有自己的虚拟机索性就用自己的机器作为编译执行机。
进入“Settings->CI/CD->Runners”展开后能看到特定的运行机和共享的运行机，关闭“Enable shared runners for this project”，在自己的云主机中安装“gitlab-runner”并配置token。

![指定执行机](/images/posts/gitlab/指定执行机.png)


## 2、运行Pipelines

### 2.1、处理ruby依赖问题

Ruby的配置过程真的是各种抓狂，记录几个可能出现的报错以及我的解决方式。

（1）如果出现killed。可能是内存不足，临时用交换分区解决

```bash
# create a 512 Mb file
dd if=/dev/zero of=/swapfile1 bs=1024 count=524288
# assign the correct permissions
chown root:root /swapfile1
chmod 0600 /swapfile1
# initialize the file as a swap file
mkswap /swapfile1
# enable swap
swapon /swapfile1

#构建完成后禁用并删除交换分区
swapoff /swapfile1
rm /swapfile1
```
（2）提示“/usr/lib/rpm/redhat/redhat-hardened-cc1：No such file or directory”
安装redhat-rpm-config

```bash
yum install redhat-rpm-config
```
（3）其它，看日志逐个解决吧


### 2.2、运行任务

创建Pipeline，等着任务处于“passed”，可能会话几分钟吧，任务状态可以从jobs中点对应得任务号进去看日志，然后根据日志排错。
![执行机](/images/posts/gitlab/指定执行机.png)


## 3、访问网站

任务编译完成后就可以直接通过 ""xxx.gitlab.io"访问博客了。如果有自己的域名，则改一下域名配置。
进入“Settings->Pages”点击 “New Domain” 输入自己的域名，再去对应的域名解析网站，添加3条记录

（1）CNAME记录

把自己的域名，解析成 xxx.gitlab.io

（2）TXT 记录

把gitlab给出的记录信息加到域名网站中，用来确认域名是你自己的，添加完成后点击“Retry”，完成确认后等几分钟便可以用https://<你自己的域名>去访问博客了。

![TXT记录](/images/posts/gitlab/TXT记录.png)


## 4、本地编译器

我习惯用vs code写博客，搭配git挺好用，直接本地编辑，git push到gitlab，触发编译动态刷新网页。怎么配就不写了，挺简单的网上搜索一大把。